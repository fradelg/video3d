# Point Cloud Fusion from 2 Kinects

This repository holds the files developed to achieve a complete 3D real-time fusion of two point clouds from two Kinect v1. The development is heavily based on ROS capabilities to capture streams in bag files (from which they are played later). The main directories contains the following files:

* launch: some scripts to launch the ROS nodes needed for some test and to fuse two point clouds
* scripts: python code to modify bags. They allows to modify topic names, messages time and bag length
* src: ROS based code in C++ to compute quaternion from matrix, (convert PCD to PLY without NaN) and merge two point cloud in one file

## Computing transformations between two point cloud using MeshLab

The transformation matrix has been computed by performing a manual alignment between a pair of point clouds. To achieve this we have manually selected four pairs of points in both point cloud to compute a rigid transformation (without considering scale). The resulting matrix is:

$$
\begin{array}{cccc}
 0.85 &  0.06 &  0.53 & -1.28 \\
-0.09 &  1.00 &  0.02 &  0.15 \\
-0.52 & -0.06 &  0.85 &  0.56 \\
 0.00 &  0.00 &  0.00 &  1.00
\end{array}
$$

This matrix can be converted into a quaternion using the following formulas:

$$
qw = sqrl(1 + M_{00} + M_{11} + M_{22}) / 2 \\
qx = (M_{21} - M_{12}) / (4 * qw) \\
qy = (M_{02} - M_{20}) / (4 * qw) \\
qz = (M_{10} - M_{01}) / (4 * qw)
$$

which results in the following values of the components

$$
qw = sqrt(1.0 + 0.85 + 1.0 + 0.85) / 2.0 = 0.9617 \\
qx = (-0.06 - 0.02) / (4 * 0.9617) = -0.0207 \\
qy = (0.53 + 0.52 ) / (4 * 0.9617) = 0.2729 \\
qz = (-0.09 - 0.06) / (4 * 0.9617) = -0.0389
$$

Then, the resulting quaternion is the next 4-dimensional array

$$ [-0.0207 \ 0.2729 \ -0.0389 \ 0.9617] $$
