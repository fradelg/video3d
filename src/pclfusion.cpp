#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/filter.h>

ros::Publisher pub;

void callback(const sensor_msgs::PointCloud2ConstPtr& pcl,
              const sensor_msgs::PointCloud2ConstPtr& pcr)
{
    pcl::PointCloud<pcl::PointXYZRGB> pcl_cloud, pcr_cloud, pcr2_cloud, output_cloud;
    pcl::fromROSMsg<pcl::PointXYZRGB>(*pcl, pcl_cloud);
    pcl::fromROSMsg<pcl::PointXYZRGB>(*pcr, pcr_cloud);

    Eigen::Matrix4d transformation;
    transformation << 0.90, -0.02, -0.43, 1.16, 0.02, 1.00, 0.00, -0.24, 0.43, -0.01, 0.90, 0.02, 0.00, 0.00, 0.00, 1.00;
    pcl::transformPointCloud(pcr_cloud, pcr2_cloud, transformation);
    output_cloud = pcl_cloud + pcr2_cloud;

    if (pub.getNumSubscribers() > 0)
        pub.publish(output_cloud);
}

void callbackCloud(const sensor_msgs::PointCloud2ConstPtr& pcl)
{
  ROS_INFO("Received a new point cloud of size %dx%d", pcl->width, pcl->height);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "merge_clouds");

    ros::NodeHandle nh;
    message_filters::Subscriber<sensor_msgs::PointCloud2> pcl_sub(nh, "points_left", 1);
    message_filters::Subscriber<sensor_msgs::PointCloud2> pcr_sub(nh, "points_right", 1);
    message_filters::TimeSynchronizer<sensor_msgs::PointCloud2,
                                      sensor_msgs::PointCloud2> sync(pcl_sub, pcr_sub, 10);
    //sync.registerCallback(boost::bind(&callback, _1, _2));
    ros::Subscriber sub = nh.subscribe("points_left", 1, callbackCloud);

    // Create a ROS publisher for the output point cloud
    pub = nh.advertise<pcl::PointCloud<pcl::PointXYZRGB> >("output", 1);

    ros::spin();
}
