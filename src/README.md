# pclfusion

Merge two point cloud from two non-synced subscriptions (the nearest ones)

* It is still a work in progress

# pcd2ply

Convert a point cloud from PCD format (Point Cloud Library) to PLY

* Taken from the [pcd2ply](https://github.com/PointCloudLibrary/pcl/blob/master/tools/pcd2ply.cpp) example from PCL code
* Remove NaN points from kinect to avoid MeshLab errors

# mat2quad

Convert a 3x3 rotation matrix to quaternion using Eigen3
