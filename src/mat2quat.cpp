#include <iostream>
#include <string>
#include <eigen3/Eigen/Geometry>

void printHelp(int, char** argv)
{
    std::cout << "Usage: mat2quad <9 matrix coefficients>" << std::endl;
}

// 0.85 0.06 0.53 -0.09 1.00 0.02 -0.52 -0.06 0.85
int main(int argc, char** argv)
{
  if (argc < 10) {
      printHelp(argc, argv);
      return (-1);
  }

  Eigen::Matrix3f rot;
  for (unsigned int i = 0; i < 3; ++i)
    for (unsigned int j = 0; i < 3; ++i)
      rot(i, j) = std::atof(argv[3*i + j + 1]);

  Eigen::Quaternion<float> q;
  q = rot;
  std::cout << q.x() << " " << q.y() << " " << q.z() << " " << q.w() << std::endl;
}
