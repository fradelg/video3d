import sys
import rosbag

if len(sys.argv) < 3:
  sys.exit('Usage: %s <input-bag-file> <number-messages>', sys.argv[0])

num_msgs = int(sys.argv[2])
with rosbag.Bag('output.bag', 'w') as outbag:
  for topic, msg, t in rosbag.Bag(sys.argv[1]).read_messages():  
    if num_msgs < 1:
      break
    num_msgs -= 1
    outbag.write(topic, msg, t)
