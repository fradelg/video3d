#!/usr/bin/env python
import sys
import rospy
import rosbag
import string

if len(sys.argv) < 2:
  sys.exit('Usage: %s <newtopic> <newframeid> <input-bag-file> <output-bag-file>', sys.argv[0])

num_msgs = 10
newtopic = sys.argv[1]
frame_id = sys.argv[2]
inbag = sys.argv[3]
outbag = sys.argv[4]

mactime = rospy.Time(1432222463, 685194303) #1432222463.687 sec
xeotime = rospy.Time(1432222454, 135759362) #1432222454.137 sec
# Compute Duration of time offset
diftime = mactime - xeotime

with rosbag.Bag(outbag, 'w') as outbag:
  for topic, msg, t in rosbag.Bag(inbag).read_messages():
    if msg._type == 'sensor_msgs/CameraInfo' or msg._type == 'sensor_msgs/Image':
      msg.header.frame_id = frame_id

    names = topic.split('/')
    names[1] = newtopic
    outbag.write('/'.join(names), msg, t + diftime)
