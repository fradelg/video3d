import sys
import rospy
import rosbag

if len(sys.argv) < 2:
  sys.exit('Usage: %s <input-bag-file> <delay>', sys.argv[0])

delay = float(sys.argv[2])
with rosbag.Bag('output.bag', 'w') as outbag:
  for topic, msg, t in rosbag.Bag(sys.argv[1]).read_messages():
    outbag.write(topic, msg, rospy.Time.from_sec(t.to_sec() + delay))
